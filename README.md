# SIT Sudan

Implementing the Sterile Insect Technique for Integrated Control of _Anopheles arabiensis_ - Phase III

Analysis of data from a suppression trial.

Observations include surveys of adults, larvae and swarms.

## Reports:

- Clean up [[HTML](https://umr-astre.pages.mia.inra.fr/sit-sudan/cleanup.html)] [[PDF](https://umr-astre.pages.mia.inra.fr/sit-sudan/cleanup.pdf)]

- Descriptive data analysis [[HTML](https://umr-astre.pages.mia.inra.fr/sit-sudan/sit-sudan.html)] [[PDF](https://umr-astre.pages.mia.inra.fr/sit-sudan/sit-sudan.pdf)]

- Mission report Khartoum, 2023-03-04/09 [[PDF](https://umr-astre.pages.mia.inra.fr/sit-sudan/mission_report.pdf)]

- Suppression analysis [[HTML](https://umr-astre.pages.mia.inra.fr/sit-sudan/suppression.html)] [[PDF](https://umr-astre.pages.mia.inra.fr/sit-sudan/suppression.pdf)]


## Output

Data is confidential and the access is currently restricted.

- [Raw data sets](https://dataverse.cirad.fr/dataset.xhtml?persistentId=doi%3A10.18167%2FDVN1%2FJ9R2NK&version=DRAFT)

- [Clean data sets](https://dataverse.cirad.fr/dataset.xhtml?persistentId=doi%3A10.18167%2FDVN1%2FMBGLUA&version=DRAFT#)

- [Meta-data](https://umr-astre.pages.mia.inra.fr/sit-sudan/metadata.html)
