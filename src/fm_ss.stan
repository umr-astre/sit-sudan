// generated with brms 2.19.0
functions {
  
}
data {
  int<lower=1> N; // total number of observations
  array[N] int Y; // response variable
  // data for splines
  int Ks; // number of linear effects
  matrix[N, Ks] Xs; // design matrix for the linear effects
  // data for spline s(day_experiment, k = 6)
  int nb_1; // number of bases
  array[nb_1] int knots_1; // number of knots
  // basis function matrices
  matrix[N, knots_1[1]] Zs_1_1;
  vector[N] offsets;
  int prior_only; // should the likelihood be ignored?
}
transformed data {
  
}
parameters {
  real Intercept; // temporary intercept for centered predictors
  vector[Ks] bs; // spline coefficients
  // parameters for spline s(day_experiment, k = 6)
  // standarized spline coefficients
  vector[knots_1[1]] zs_1_1;
  real<lower=0> sds_1_1; // standard deviations of spline coefficients
  real<lower=0> shape; // shape parameter
}
transformed parameters {
  // actual spline coefficients
  vector[knots_1[1]] s_1_1;
  real lprior = 0; // prior contributions to the log posterior
  // compute actual spline coefficients
  s_1_1 = sds_1_1 * zs_1_1;
  lprior += normal_lpdf(Intercept | 9, 2.5);
  lprior += normal_lpdf(bs | 0, 0.0087);
  lprior += exponential_lpdf(sds_1_1 | 1);
  lprior += exponential_lpdf(shape | 1.9);
}
model {
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N] mu = rep_vector(0.0, N);
    mu += Intercept + Xs * bs + Zs_1_1 * s_1_1 + offsets;
    target += neg_binomial_2_log_lpmf(Y | mu, shape);
  }
  // priors including constants
  target += lprior;
  target += std_normal_lpdf(zs_1_1);
}
generated quantities {
  // actual population-level intercept
  real b_Intercept = Intercept;
  // additionally sample draws from priors
  real prior_Intercept = normal_rng(9, 2.5);
  real prior_bs = normal_rng(0, 0.0087);
  real prior_sds_1_1 = exponential_rng(1);
  real prior_shape = exponential_rng(1.9);
  // use rejection sampling for truncated priors
  while (prior_sds_1_1 < 0) {
    prior_sds_1_1 = exponential_rng(1);
  }
  while (prior_shape < 0) {
    prior_shape = exponential_rng(1.9);
  }
}

