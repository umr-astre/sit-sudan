
xexp <- rexp(1e4, rate = 0.7)

sqrtexp <- sqrt(xexp)

par(mfcol = c(2, 1))
hist(xexp, xlim = c(0, 12))
hist(sqrtexp, xlim = c(0, 12))

prior_dat <-
  prior_draws(fm_as, variable = c('b_rolesit', 'sd_sector')) |>
  as_tibble() |>
  mutate(
    r_sectorsit = rnorm(n = n(), sd = sd_sector)
  )

prior_dat |>
  ggplot() +
  aes(r_sectorsit, b_rolesit) +
  geom_abline(intercept = 0, slope = -1, colour = 'darkgrey') +
  geom_hex(show.legend = FALSE) +
  coord_fixed() +
  labs(
    x = "Sector effect",
    y = "Treatment effect"
  ) +
  scale_fill_viridis_c() +
  theme_ipsum(grid = "XY")

prior_dat |>
  pivot_longer(
    cols = c('r_sectorsit', 'b_rolesit'),
    names_to = 'var',
    values_to = 'value'
  ) |>
  ggplot() +
  aes(value, colour = var) +
  geom_density()

mean(prior_dat$sd_sector) # ~ 1.5 = s0
1/mean(prior_dat$sd_sector) # ~ .7 = 1/s0
sd(prior_dat$r_sectorsit)       # ~ 1.9
sd(prior_dat$b_rolesit)   # ~ 7 = 2.5*s0/sd(sit)
1/var(prior_dat$r_sectorsit)       # ~ 2

## The double-exponential (Laplace) is equivalent to a
## Normal distribution with exponential variance
ralpha <- rexp(4e4, rate = .7)
rx <- rnorm(4e4, sd = sqrt(ralpha))
sd(rx)
1/var(rx)  # ~ rate

## But the random effects follow a Normal distribution
## with exponential Standard Deviation (not variance)
rgamma <- rexp(4e4, rate = .7)
ry <- rnorm(4e4, sd = rgamma)
sd(ry)
sqrt(2)/.7
