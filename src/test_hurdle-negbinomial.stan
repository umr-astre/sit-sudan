// generated with brms 2.19.0
functions {
  /* hurdle negative binomial log-PDF of a single response
   * Args:
   *   y: the response value
   *   mu: mean parameter of negative binomial distribution
   *   phi: shape parameter of negative binomial distribution
   *   hu: hurdle probability
   * Returns:
   *   a scalar to be added to the log posterior
   */
  real hurdle_neg_binomial_lpmf(int y, real mu, real phi, real hu) {
    if (y == 0) {
      return bernoulli_lpmf(1 | hu);
    } else {
      return bernoulli_lpmf(0 | hu) + neg_binomial_2_lpmf(y | mu, phi)
             - log1m((phi / (mu + phi)) ^ phi);
    }
  }
  /* hurdle negative binomial log-PDF of a single response
   * logit parameterization for the hurdle part
   * Args:
   *   y: the response value
   *   mu: mean parameter of negative binomial distribution
   *   phi: phi parameter of negative binomial distribution
   *   hu: linear predictor of hurdle part
   * Returns:
   *   a scalar to be added to the log posterior
   */
  real hurdle_neg_binomial_logit_lpmf(int y, real mu, real phi, real hu) {
    if (y == 0) {
      return bernoulli_logit_lpmf(1 | hu);
    } else {
      return bernoulli_logit_lpmf(0 | hu) + neg_binomial_2_lpmf(y | mu, phi)
             - log1m((phi / (mu + phi)) ^ phi);
    }
  }
  /* hurdle negative binomial log-PDF of a single response
   * log parameterization for the negative binomial part
   * Args:
   *   y: the response value
   *   eta: linear predictor for negative binomial distribution
   *   phi phi parameter of negative binomial distribution
   *   hu: hurdle probability
   * Returns:
   *   a scalar to be added to the log posterior
   */
  real hurdle_neg_binomial_log_lpmf(int y, real eta, real phi, real hu) {
    if (y == 0) {
      return bernoulli_lpmf(1 | hu);
    } else {
      return bernoulli_lpmf(0 | hu) + neg_binomial_2_log_lpmf(y | eta, phi)
             - log1m((phi / (exp(eta) + phi)) ^ phi);
    }
  }
  /* hurdle negative binomial log-PDF of a single response
   * log parameterization for the negative binomial part
   * logit parameterization for the hurdle part
   * Args:
   *   y: the response value
   *   eta: linear predictor for negative binomial distribution
   *   phi: phi parameter of negative binomial distribution
   *   hu: linear predictor of hurdle part
   * Returns:
   *   a scalar to be added to the log posterior
   */
  real hurdle_neg_binomial_log_logit_lpmf(int y, real eta, real phi, real hu) {
    if (y == 0) {
      return bernoulli_logit_lpmf(1 | hu);
    } else {
      return bernoulli_logit_lpmf(0 | hu)
             + neg_binomial_2_log_lpmf(y | eta, phi)
             - log1m((phi / (exp(eta) + phi)) ^ phi);
    }
  }
  // hurdle negative binomial log-CCDF and log-CDF functions
  real hurdle_neg_binomial_lccdf(int y, real mu, real phi, real hu) {
    return bernoulli_lpmf(0 | hu) + neg_binomial_2_lccdf(y | mu, phi)
           - log1m((phi / (mu + phi)) ^ phi);
  }
  real hurdle_neg_binomial_lcdf(int y, real mu, real phi, real hu) {
    return log1m_exp(hurdle_neg_binomial_lccdf(y | mu, phi, hu));
  }
}
data {
  int<lower=1> N; // total number of observations
  array[N] int Y; // response variable
  int<lower=1> K; // number of population-level effects
  matrix[N, K] X; // population-level design matrix
  int<lower=1> K_hu; // number of population-level effects
  matrix[N, K_hu] X_hu; // population-level design matrix
  int prior_only; // should the likelihood be ignored?
}
transformed data {
  int Kc = K - 1;
  matrix[N, Kc] Xc; // centered version of X without an intercept
  vector[Kc] means_X; // column means of X before centering
  int Kc_hu = K_hu - 1;
  matrix[N, Kc_hu] Xc_hu; // centered version of X_hu without an intercept
  vector[Kc_hu] means_X_hu; // column means of X_hu before centering
  for (i in 2 : K) {
    means_X[i - 1] = mean(X[ : , i]);
    Xc[ : , i - 1] = X[ : , i] - means_X[i - 1];
  }
  for (i in 2 : K_hu) {
    means_X_hu[i - 1] = mean(X_hu[ : , i]);
    Xc_hu[ : , i - 1] = X_hu[ : , i] - means_X_hu[i - 1];
  }
}
parameters {
  vector[Kc] b; // population-level effects
  real Intercept; // temporary intercept for centered predictors
  real<lower=0> shape; // shape parameter
  vector[Kc_hu] b_hu; // population-level effects
  real Intercept_hu; // temporary intercept for centered predictors
}
transformed parameters {
  real lprior = 0; // prior contributions to the log posterior
  lprior += student_t_lpdf(Intercept | 3, -2.3, 2.5);
  lprior += gamma_lpdf(shape | 0.01, 0.01);
  lprior += logistic_lpdf(Intercept_hu | 0, 1);
}
model {
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N] mu = rep_vector(0.0, N);
    // initialize linear predictor term
    vector[N] hu = rep_vector(0.0, N);
    mu += Intercept + Xc * b;
    hu += Intercept_hu + Xc_hu * b_hu;
    for (n in 1 : N) {
      target += hurdle_neg_binomial_log_logit_lpmf(Y[n] | mu[n], shape, hu[n]);
    }
  }
  // priors including constants
  target += lprior;
}
generated quantities {
  // actual population-level intercept
  real b_Intercept = Intercept - dot_product(means_X, b);
  // actual population-level intercept
  real b_hu_Intercept = Intercept_hu - dot_product(means_X_hu, b_hu);
}
