#' Knitr output format
#'
#' Returns either 'html' or 'pdf' depending on the current output format
#'
#' @example
#' htmlorpdf()
htmlorpdf <- function() {
  ifelse(knitr::is_html_output(), "html", "pdf")
}


#' Last release date
#'
#' Retrieve the last date where a release has been
#' performed, prior to give dates x.
#'
#' @param x Vector of dates.
#' @param release_dates Vector of dates.
#'
#' @return Vector of dates of the same length as x.
#'
#' @examples
#' last_release_date(
#'   x = as.Date(c('2019-01-01', '2015-01-01', '2020-01-01')),
#'   release_dates = as.Date(c('2017-06-01', '2019-06-01', '2019-11-01'))
#' )
#'
last_release_date <- function(x, release_dates) {
  # x <- sort(sample(unique(adult_catches$date), size = 5))
  # release_dates <- releases$date

  last_date <- function(x1, dates) {
    # x1 <- x[[1]]
    # dates <- release_dates
    ans <- tail(dates[x1 > dates], 1)

    if(length(ans) == 0) ans <- NA

    return(ans)
  }

  as.Date(vapply(x, last_date, Date(1), release_dates))
}



#' Estimator of population size
#'
#' Chapman estimator of population size for a simple recapture procedure.
#'
#' M individuals are captured from a closed population, marked and released.
#' In a recapture, the sample contains m marked and n unmarked individuals.
#'
#' The maximum-likelihood estimate of the population size is then (m+n) * M / m
#' (Petersen estimator). However, this does not have finite variance and is
#' very noisy for small samples.
#'
#' Chapman introduced a correction which involves adding 1 to each terms:
#' (m + n + 1) * (M + 1) / (m + 1) - 1.
#'
#' Note that this estimator includes the marked individuals in the population.
#'
#'
#' @param M Numeric vector of released sterile males.
#' @param m Numeric vector of captured sterile males.
#' @param n Numeric vector of captured wild males.
#'
#' @return A numeric vector of population estimates with an attribute of
#'   standard errors.
#' @export
#'
#' @references Thompson, Steven K. Sampling. 3rd ed. Wiley Series in Probability
#'   and Statistics. Hoboken, N.J: Wiley, 2012. p. 265.
#' @examples
#' estimate_population_size(
#'   M = c(1, 1.5, 3) * 1e3,
#'   m = c(3, 3, 5),
#'   n = 2:4 * 1e1
#' )
estimate_population_size <- function(M, m, n) {

  p_est <- (m + n + 1) * (M + 1) / (m + 1) - 1

  p_var_num <- (m + 1) * (n + m + 1) * (M - m) * n

  p_var_denom <- (m + 1)**2 * (m + 1)

  p_sd <- sqrt(p_var_num / p_var_denom)

  return(structure(p_est, se = p_sd))
}

#' Surviving sterile individuals over time
#'
#' @examples
#' sterile_decay(0:10, 1e3, .8)
#' sterile_decay(c(-3, 3), 1e3, .8)
sterile_decay <- function(x, N, surv_rate) {
  x0 <- 1 / log(surv_rate)
  ans <- N * exp(x / x0)
  ans[x < 0] <- 0
  return(ans)
}

#' Sterile population over time
#'
#' @examples
#' x <- as.Date('2023-02-01') + 0:16
#' release_dates <- as.Date(c('2023-02-03', '2023-02-09', '2023-02-11'))
#' release_numbers <- c(1, 3, .4) * 1e3
#' sterile_population(x, release_dates, release_numbers, surv_rate = 0.8)
sterile_population <- function(x, release_dates, release_numbers, surv_rate) {

  stopifnot(
    identical(length(release_dates), length(release_numbers))
  )

  days_since_release <- function(x, r) {
    as.numeric(difftime(x, r, units = 'days'))
  }

  days_releases <- lapply(
    release_dates,
    days_since_release,
    x = x
  )

  ans_release <- mapply(
    sterile_decay,
    days_releases,
    release_numbers,
    MoreArgs = list(surv_rate = surv_rate)
  )

  ans <- rowSums(ans_release)

  return(ans)
}


compute_sterile_pressure <- function(surv_rate, dates, releases) {
  tibble(
    date = dates
  ) |>
    mutate(
      sterile_population =
        sterile_population(
          date,
          releases$date,
          releases$n,
          surv_rate = surv_rate
        ),
      sterile_pressure = log(
        pmax(1, sterile_population)
      )
    ) |>
    pivot_longer(
      cols = starts_with("sterile_"),
      names_to = "variable",
      values_to = "value"
    )
}