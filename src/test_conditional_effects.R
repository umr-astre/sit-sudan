library(tidyverse)
library(tidybayes)
library(brms)

set.seed(20230727)

tdf <- tibble(
  area = gl(2, 24),
  sector = gl(6, 8),
  x = runif(length(sector)),
  y = (2 * seq.int(2) - 3)[area] + (rep(seq.int(3), 2) - 2)[sector] + 2 * x + .1 * rnorm(length(x))
)
ggplot(tdf, aes(x, y, colour = factor(sector))) + geom_point()

tfm <- brm(
  y ~ area + (1 | sector) + x,
  data = tdf,
  backend = 'cmdstanr'
)



## Effect of the area
conditional_effects(
  tfm,
  effects = "area",
  conditions = list(x = 0),
  method = "posterior_epred",  # Default
  re_formula = NA  # Default. sector = 0
  # re_formula = NULL # More variation. Integrate the variability of sector.
)

## This is:
as.data.frame(tfm) |>
  mutate(
    a_1 = b_Intercept,
    a_2 = b_Intercept + b_area2
  ) |>
  select(starts_with('a_')) |>
  pivot_longer(
    cols = starts_with('a_'),
    names_to = 'area',
    names_prefix = 'a_',
    values_to = 'value'
  ) |>
  group_by(area) |>
  median_hdci(.width = .95) |>
  ggplot() +
  aes(value, area) +
  geom_pointrange(aes(xmin = .lower, xmax = .upper))


## The random effects of the sectors
as_tibble(
  ranef(tfm, groups = "sector")$sector[, , 'Intercept']
) |>
  mutate(
      sector = levels(tdf$sector)
  ) |>
  ggplot() +
  aes(Estimate, sector) +
  geom_pointrange(
    aes(xmin = Q2.5, xmax = Q97.5)
  )

## How can I see both effects "combined"?

## Draws from the Expected Value of the posterior predictive distribution
posterior_epred(
  tfm,
  newdata = distinct(
    tdf,
    area, sector
  ) |>
    add_column(x = 0),
  re_formula = NULL,  # Default. Include all group level effects
  ndraws = 1
)

## This gives the same draws but added to the target data.frame in a tidy way
add_epred_draws(
  newdata = distinct(
    tdf,
    area, sector
  ) |>
    add_column(x = 0),
  tfm,
  re_formula = NULL,  # Default. Include all group level effects
  # re_formula = ~sector, # Why this is not the same???
  ndraws = 4000
) |>
  median_hdci(.width = .95) |>
  ggplot() +
  aes(.epred, sector) +
  geom_pointrange(aes(xmin = .lower, xmax = .upper))


## This is equivalent to:
as.data.frame(tfm) |>
  mutate(
    s_1 = b_Intercept + `r_sector[1,Intercept]`,
    s_2 = b_Intercept + `r_sector[2,Intercept]`,
    s_3 = b_Intercept + `r_sector[3,Intercept]`,
    s_4 = b_Intercept + b_area2 + `r_sector[4,Intercept]`,
    s_5 = b_Intercept + b_area2 + `r_sector[5,Intercept]`,
    s_6 = b_Intercept + b_area2 + `r_sector[6,Intercept]`
  ) |>
  select(starts_with('s_')) |>
  pivot_longer(
    cols = starts_with('s_'),
    names_to = 'sector',
    names_prefix = 's_',
    values_to = 'value'
  ) |>
  group_by(sector) |>
  median_hdci(.width = .95) |>
  ggplot() +
  aes(value, sector) +
  geom_pointrange(aes(xmin = .lower, xmax = .upper))


## They are much more precise because the area and sector effects
## are confounded:
as.data.frame(tfm) |>
  select(b_Intercept, b_area2, starts_with('r_sector')) |>
  GGally::ggpairs()



# Nimble implementation ---------------------------------------------------


## MODEL
## y ~ N(eta, sy)
## eta = int + ge + beta * x
## ge ~ N(0, sg)
library(nimble)

ncode <- nimbleCode({
  ## Priors
  sy ~ dexp(1)
  sg ~ dexp(1)
  alpha ~ dnorm(0, 1)
  beta ~ dnorm(0, 1)

  ## Group effects
  for (i in 1:G) {
    ge[i] ~ dnorm(0, sg)
  }

  ## Likelihood
  for (i in 1:N) {
    ## Linear predictor
    eta[i] <- alpha + ge[group[i]] + beta * x[i]
    y[i] ~ dnorm(eta[i], sy)
  }

})

## Constants
ncons <- list(N = nrow(tdf), G = nlevels(tdf$group), group = as.numeric(tdf$group))

## Initial values
ninit <- function() list(
  sy = rexp(n = 1, rate = 1),
  sg = rexp(n = 1, rate = 1),
  ge = rnorm(n = 3, 0, 1),
  alpha = rnorm(n = 1, 0, 1),
  beta = rnorm(n = 1, 0, 1)
)

nft <- nimbleModel(
  code = ncode,
  constants = ncons,
  data = tdf[, -1],
  inits = ninit()
)

nfm <- nimbleMCMC(
  code = ncode,
  constants = ncons,
  data = list(y = tdf$y, x = tdf$x),
  inits = ninit(),
  monitors = c("sy", "sg", "ge", "alpha", "beta"),
  niter = 1000,
  nburnin = 100,
  nchains = 4
)


## Bug report
## https://github.com/nimble-dev/nimble/issues/1335
library(reprex)

library(nimble)
code <- nimbleCode({
  int ~ dexp(1)
})
rModel <- nimbleModel(code)
cModel <- compileNimble(rModel)
printErrors()
