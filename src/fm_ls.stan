// generated with brms 2.19.0
functions {
  
}
data {
  int<lower=1> N; // total number of observations
  vector[N] Y; // response variable
  int<lower=1> K; // number of population-level effects
  matrix[N, K] X; // population-level design matrix
  // data for splines
  int Ks; // number of linear effects
  matrix[N, Ks] Xs; // design matrix for the linear effects
  // data for spline s(day_experiment, k = 6)
  int nb_1; // number of bases
  array[nb_1] int knots_1; // number of knots
  // basis function matrices
  matrix[N, knots_1[1]] Zs_1_1;
  vector[N] offsets;
  // data for group-level effects of ID 1
  int<lower=1> N_1; // number of grouping levels
  int<lower=1> M_1; // number of coefficients per level
  array[N] int<lower=1> J_1; // grouping indicator per observation
  // group-level predictor values
  vector[N] Z_1_1;
  int prior_only; // should the likelihood be ignored?
}
transformed data {
  int Kc = K - 1;
  matrix[N, Kc] Xc; // centered version of X without an intercept
  vector[Kc] means_X; // column means of X before centering
  for (i in 2 : K) {
    means_X[i - 1] = mean(X[ : , i]);
    Xc[ : , i - 1] = X[ : , i] - means_X[i - 1];
  }
}
parameters {
  vector[Kc] b; // population-level effects
  real Intercept; // temporary intercept for centered predictors
  vector[Ks] bs; // spline coefficients
  // parameters for spline s(day_experiment, k = 6)
  // standarized spline coefficients
  vector[knots_1[1]] zs_1_1;
  real<lower=0> sds_1_1; // standard deviations of spline coefficients
  real<lower=0> sigma; // dispersion parameter
  vector<lower=0>[M_1] sd_1; // group-level standard deviations
  array[M_1] vector[N_1] z_1; // standardized group-level effects
}
transformed parameters {
  // actual spline coefficients
  vector[knots_1[1]] s_1_1;
  vector[N_1] r_1_1; // actual group-level effects
  real lprior = 0; // prior contributions to the log posterior
  // compute actual spline coefficients
  s_1_1 = sds_1_1 * zs_1_1;
  r_1_1 = sd_1[1] * z_1[1];
  lprior += double_exponential_lpdf(b[1] | 0, 1.9);
  lprior += normal_lpdf(b[2] | 0, 10);
  lprior += normal_lpdf(b[3] | 0, 10);
  lprior += normal_lpdf(b[4] | 0, 2.5);
  lprior += normal_lpdf(Intercept | 1.32, 4.76);
  lprior += normal_lpdf(bs[1] | 0, 0.016);
  lprior += exponential_lpdf(sds_1_1 | 0.53);
  lprior += exponential_lpdf(sigma | .53);
  lprior += exponential_lpdf(sd_1 | 0.53);
}
model {
  // likelihood including constants
  if (!prior_only) {
    // initialize linear predictor term
    vector[N] mu = rep_vector(0.0, N);
    mu += Intercept + Xc * b + Xs * bs + Zs_1_1 * s_1_1 + offsets;
    for (n in 1 : N) {
      // add more terms to the linear predictor
      mu[n] += r_1_1[J_1[n]] * Z_1_1[n];
    }
    target += lognormal_lpdf(Y | mu, sigma);
  }
  // priors including constants
  target += lprior;
  target += std_normal_lpdf(zs_1_1);
  target += std_normal_lpdf(z_1[1]);
}
generated quantities {
  // actual population-level intercept
  real b_Intercept = Intercept - dot_product(means_X, b);
  // additionally sample draws from priors
  real prior_b__1 = double_exponential_rng(0, 1.9);
  real prior_b__2 = normal_rng(0, 10);
  real prior_b__3 = normal_rng(0, 10);
  real prior_b__4 = normal_rng(0, 2.5);
  real prior_Intercept = normal_rng(1.32, 4.76);
  real prior_bs__1 = normal_rng(0, 0.016);
  real prior_sds_1_1 = exponential_rng(0.53);
  real prior_sigma = exponential_rng(.53);
  real prior_sd_1 = exponential_rng(0.53);
  // use rejection sampling for truncated priors
  while (prior_sds_1_1 < 0) {
    prior_sds_1_1 = exponential_rng(0.53);
  }
  while (prior_sigma < 0) {
    prior_sigma = exponential_rng(.53);
  }
  while (prior_sd_1 < 0) {
    prior_sd_1 = exponential_rng(0.53);
  }
}

